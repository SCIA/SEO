import sys
from utils import *

def main():
    csv = False
    if len(sys.argv) < 3:
        print("language.py: ERROR: language.py need 2 arguments to work: first is the folder with the corpus, second is the size of the Ngram", file=sys.stderr)
        printHelp()
        exit(1)

    if len(sys.argv) > 3 and sys.argv[3] == '--csv':
        csv = True
    if csv:
        print("Computing TFIDF of files in " + sys.argv[1] + " with " + sys.argv[2] + "-ngrams")
    else:
        print("Computing TFIDF of files in " + sys.argv[1] + " with " + sys.argv[2] + "-ngrams", file=sys.stderr)

    ngramsTotalAppearance, nbFiles, filesNgrams = computeOccurences(sys.argv[1], int(sys.argv[2]))
    res = computeTFIDFs(sys.argv[1], ngramsTotalAppearance, nbFiles, filesNgrams, csv)
    print(res)
    if len(sys.argv) > 3 and sys.argv[3] == '--json':
         print(str(res).replace('\'', '\"'))

    print("Finished computations", file=sys.stderr)

def printHelp():
    print("\n\tpython tfidf.py <corpus folder> <Ngram length> [OPTIONS]")
    print("\n\t--csv\t\t\t\t\tprint a CSV format result on stdout")
    print("\n\t--json\t\t\t\t\tprint a JSon format result on stdout")

if __name__ == "__main__":
     main()

import math
import os
import re
import sys

def mergeDict(dict1, dict2):
    for ngram in dict2.keys():
        dict1[ngram] = dict1.get(ngram, 0) + 1
    return dict1

def tokenize(text):
    text = text.lower()
    without_eol = re.sub(r"\\r|\\n", "", text)
    without_numbers = re.sub(r"\w*[0-9]\w*", "", without_eol)
    return re.findall(r"<a.*?/a>|[\w]+", without_numbers)

def ngrams(text, size): #by character for language detection
    res = {}
    for i in range(len(text) - size):
        ngram = text[i]
        for j in range(1, size):
            ngram += " " + text[i + j]
        res[ngram] = res.get(ngram, 0) + 1
    return res

def computeOccurences(path, sizeNgram):
    res = {}
    if os.path.isfile(path):
        print("tfidf.py: ERROR: need a folder path with all the text", file=sys.stderr)
        exit(1)
    files = os.listdir(path)
    filesNgrams = {}
    nb_file = 0
    for file in files:
        pathFile = path + "/" + file
        if os.path.isfile(pathFile):
            try:
                with open(pathFile, "r", encoding="utf-8") as f:
                    fileContent = f.read()
            except:
                print("language.py: ERROR: file cannot be open: " + pathFile,file=sys.stderr)
                pass
            currentFileNgrams = ngrams(tokenize(fileContent), sizeNgram)
            filesNgrams[file] = currentFileNgrams
            res = mergeDict(res, currentFileNgrams)
            nb_file += 1
    return res, nb_file, filesNgrams

def computeTFIDFs(folderPath, ngramsTotalAppearance, nbFiles, filesNgrams, csvPrint):
    res = {}
    for file in os.listdir(folderPath):
        if csvPrint:
            print("Starting computation of: " + file)
        else:
            print("Starting computation of: " + file, file=sys.stderr)
        keys = filesNgrams[file].keys()
        numberOfNgram = len(keys)
        curFileTFIDF = {}
        for ngram in keys:
            tf = filesNgrams[file][ngram] / numberOfNgram
            idf = math.log(nbFiles / ngramsTotalAppearance[ngram], 10)
            curFileTFIDF[ngram] = tf * idf
            if csvPrint:
                print(ngram + "," + str(tf * idf))
        res[file] = curFileTFIDF
    return res

import sys
from utils import *
from languagesHandler import *

def main():
    csv = False
    if len(sys.argv) < 4:
        print("language.py: ERROR: language.py need 3 arguments to work: first is the folder with the corpus, second is the folder that contains all the language, third is the number of ngram to use for prediction ", file=sys.stderr)
        printHelp()
        exit(1)

    print("Computing TFIDF of files in " + sys.argv[1] + " with " + sys.argv[2] + "-ngrams", file=sys.stderr)

    ngram = int(sys.argv[3])
    tfidfs = []
    for i in range(1, ngram + 2):
        ngramsTotalAppearance, nbFiles, filesNgrams = computeOccurences(sys.argv[1], i)
        tfidfs.append(computeTFIDFs(sys.argv[1], ngramsTotalAppearance, nbFiles, filesNgrams, False))


    print("Finished computations", file=sys.stderr)

    while True:
        strToParse = input("Enter the beggining of your sentence (enter \"STOP\" to close the program)\n").lower()
        if strToParse == "stop":
            return
        language = languageDetector(sys.argv[2], strToParse, "")
        strToParse = strToParse.split(" ")
        if len(strToParse) > ngram:
            strToParse = strToParse[-ngram:]
        validRes = {}
        for file, dico in tfidfs[len(strToParse)].items():
            for key, value in dico.items():
                keySplit = key.split(" ")
                i = 0
                cmp = True
                while i < len(strToParse) and cmp:
                    if keySplit[i] != strToParse[i]:
                        cmp = False
                    i += 1
                if cmp:
                    prev = validRes.get(keySplit[-1], 0)
                    validRes[keySplit[-1]] = value + prev
        validRes = dict(sorted(validRes.items(), key=lambda x: x[1])[::-1])
        print("Language used: " + language)
        if len(validRes) > 0:
            cmp = 5
            print("List of possible word to add:")
            for key in validRes.keys():
                if cmp == 0:
                    break
                print("\t" + key)
                cmp -= 1
        else:
            print("No prediction could be done")


def printHelp():
    print("\n\tpython tfidf.py <corpus folder> <languages folder> <Ngram length>")

if __name__ == "__main__":
     main()

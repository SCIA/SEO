import os
from collections import OrderedDict

class Language:
    def __init__(self, name, totalNgram, ngrams):
            self.name = name
            self.totalNgram = totalNgram
            self.ngrams = ngrams
    def __str__(self):
        return self.name + " - "+ str(self.totalNgram) +"\n"+ str(self.ngrams)

def mergeDict(dict1, dict2):
    for ngram in dict2.keys():
        dict1[ngram] = dict1.get(ngram, 0) + dict2[ngram]
    return dict1

def ngrams(text, size): #by character for language detection
    res = {}
    for i in range(len(text)):
        if size == 1 and (text[i] == ' ' or text[i] == '\n'):
            continue
        res[text[i:i + size]] = res.get(text[i:i + size], 0) + 1
    return res

def loadLanguages(path):
    if os.path.isfile(path):
        return {}
    files = os.listdir(path)
    languageInfos = []
    for file in files:
        pathFile = path + "/" + file
        if os.path.isfile(pathFile):
            with open(pathFile, "r", encoding="utf-8") as f:
                content = f.read()
            name = content[-8:-1][content[-8:-1].find(':') + 1:].replace("\"", "")
            totalNgram = [int(s) for s in content[content.find('[') + 1: content.find(']')].split(',')]
            split = [s.replace("\"", "") for s in content[content.find('{', 1) + 1: content.find('}')].split(',')]
            ngrams = {}
            for s in split:
                doubleDot = s.find(':')
                ngrams[s[:doubleDot]] = int(s[doubleDot + 1:])
            languageInfos.append(Language(name, totalNgram, ngrams))
    return languageInfos

def vectDist(probaForText, probaForLanguage):
    sum = 0
    currentIndex = 0
    valueWhenNotFound = 5000
    for ngram in probaForText.keys():
        try:
            sum += abs(list(probaForLanguage.keys()).index(ngram) - currentIndex)
        except:
            sum += valueWhenNotFound
        currentIndex += 1
    return sum

def languageDetector(languagesFolder, fileContent, option):
    textNgrams = {}
    totalNgramInText = []
    for i in range(1,4):
        tmpNgrams = ngrams(fileContent.lower(), i)
        totalNgramInText.append(sum(tmpNgrams.values()))
        textNgrams = mergeDict(textNgrams, tmpNgrams)

    probaForText = {}
    for n in textNgrams:
        probaForText[n] = textNgrams[n] / totalNgramInText[len(n) - 1]

    bestLanguage = Language("", [], {})
    bestLanguageDist = -1
    for l in loadLanguages(languagesFolder):
        probaForLanguage = {}
        for n in l.ngrams.keys():
            probaForLanguage[n] = l.ngrams[n] / l.totalNgram[len(n) - 1]

        currentLanguageDist = vectDist(dict(sorted(probaForText.items(), key=lambda x: x[1])[::-1]), dict(sorted(probaForLanguage.items(), key=lambda x: x[1])[::-1]))

        if option == '-v' or option == '--verbose':
            print("Distance of " + str(currentLanguageDist) + " for " + l.name)
        elif option == '--csv':
            print(l.name + "," + str(currentLanguageDist))

        if bestLanguageDist < 0 or currentLanguageDist < bestLanguageDist:
            bestLanguageDist = currentLanguageDist
            bestLanguage = l
    return bestLanguage.name

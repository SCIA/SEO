AUTHORS
=======

BERTRAIT Maxime - maxime.bertrait@epita.fr
DURAND Adrien - adrien.durand@epita.fr
FAURE Sebastien - sebastien.faure@epita.fr

PROJECT
=======

The goal of this project is to create a writing helper system which will take a set of texts
and will extract the most significant n-grams.
Then, the program will use the TF-IDF method to class these n-grams.
The solution is able to work on several languages and detect them automatically

RUN
===

 $ python tfidf/tfidf.py <corpus folder> <length of ngrams> [OPTIONS]

 $ python languageDetector/languageDetector.py <languages folder> <file or folder to test> [OPTIONS]

 $ python tfidf/prompt.py <corpus folder> <languages folder> <length of Ngram>

TEST
====

TEST FOR LANGUAGE DETECTOR:
Files in languagesDetectorTestFiles folder. Names must end with the language code
and be at least 5 characters long.
To test it, at the root of this project, launch:
  $ ./testLanguageDetector.sh

Use testLanguageDetector.py if you are on Windows

TEST FOR TFIDF:
$ ./testTFIDF.sh

Use testTFIDF.py if you are on Windows

OPTIONS
=======

LANGUAGE DETECTOR:
  --csv           give you the score of languages handle for the given text

TFIDF:
  --csv           give you the TFIDF score for each ngrams of each text following CSV format
  --json          give you the TFIDF score for each ngrams of each text following JSON format

Both result are print on the standard output. Just redirect it to your file to save the result:
  $ <program call> 1> <your file path>
such as:
  $ python tfidf.py ./corpusMotorcycle 3 --json 1> resultFile.json

PROMPT
======

You can run a prompt in the folder tfidf that is used as followed:
  $ python prompt.py ./corpusMotorcycle ../languageDetector/shortLanguages/ 2

This will parse the corpus and then you can enter the beginning of a sentence
and it will give you what you could write next. It also gives you the language
of the sentence that you wrote.

Improvement: This prompt could be improved by having different corpus with
different languages that are parsed to give the proposition accordingly to
the language used by the user.

import sys
import operator
import os
from languagesHandler import *
from collections import OrderedDict

def main():
    option = ""
    if len(sys.argv) < 3:
        print("language.py: ERROR: language.py need 2 arguments to work: first is the folder with the corpus, second is the file to test")
        printHelp()
        exit(1)
    if len(sys.argv) > 0 and (sys.argv[-1] == '-h' or sys.argv[-1] == '--help'):
        printHelp()
        exit(0)
    if len(sys.argv) > 3:
        option = sys.argv[3]

    path = sys.argv[2]
    if os.path.isfile(path):
        with open(path, "r", encoding="utf-8") as f:
            fileContent = f.read()
        language = languageDetector(sys.argv[1], fileContent, option)
        print("Language code: " + language)
    else:
        nbFiles = 0
        nbFound = 0
        files = os.listdir(path)
        for file in files:
            pathFile = path + "/" + file
            if os.path.isfile(pathFile):
                print("Processing " + file)
                nbFiles += 1
                with open(pathFile, "r", encoding="utf-8") as f:
                    fileContent = f.read()
                language = languageDetector(sys.argv[1], fileContent, option)
                if language.lower() in file[-len(language):].lower():
                    print("Language code: " + language)
                    nbFound += 1
                else:
                    print("Language code: " + language)
        print("Total results : " + str(nbFound) + " language found for " + str(nbFiles)
        + " which is equal to " + str((nbFound / nbFiles) * 100) + "%")
    return 0

def printHelp():
    print("\n\tpython language.py <corpus folder> <file or folder to test> [OPTIONS]")
    print("\t--csv\t\t\t\t\tsame as -v but in CSV format")
    print("\t-h, --help\t\t\t\tprint this help")
    print("\t-v, --verbose\t\t\t\tprint each language result")

if __name__ == "__main__":
     main()
